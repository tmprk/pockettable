# PocketTable (macOS)

<p align="center">
  <img src="https://github.com/tmprk/PocketTable-macOS/blob/master/periodic.png">
</p>

A convenient periodic table right in your macOS Menu Bar. Click through elements to get essential data such as atomic mass and electron configuration.

## Written in

- Swift 5
- Xcode 10.3
