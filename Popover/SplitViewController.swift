//
//  SplitViewController.swift
//  Popover
//
//  Created by Timothy Park on 6/10/19.
//  Copyright © 2019 Timothy Park. All rights reserved.
//

import Cocoa

class SplitViewController: NSSplitViewController {
    
    @IBOutlet weak var periodiTableItem: NSSplitViewItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSLayoutConstraint(item: periodiTableItem.viewController.view, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 500).isActive = true
    }    

    func toggleSidebar() {
        if splitView.isSubviewCollapsed(splitView.subviews[1] as NSView) {
            openSidebar()
        } else {
            closeSidebar()
        }
    }
    
    func closeSidebar() {
        let mainView = splitView.subviews[0] as NSView
        let sidepanel = splitView.subviews[1] as NSView
        sidepanel.isHidden = true
        let viewFrame = splitView.frame
        mainView.frame.size = NSMakeSize(viewFrame.size.width, viewFrame.size.height)
        splitView.display()
    }
    
    func openSidebar() {
        let sidepanel = splitView.subviews[1] as NSView
        sidepanel.isHidden = false
        let viewFrame = splitView.frame
        sidepanel.frame.size = NSMakeSize(viewFrame.size.width, 200)
        splitView.display()
    }
    
}
