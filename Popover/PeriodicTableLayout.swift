//
//  PeriodicTableLayout.swift
//  Popover
//
//  Created by Timothy Park on 6/7/19.
//  Copyright © 2019 Timothy Park. All rights reserved.
//

import Cocoa

protocol ElementDataSource {
    var elements: [Element]? { get }
}

class PeriodicTableLayout: NSCollectionViewFlowLayout {
    
    let rows = 9
    let columns = 18
    var size: CGSize!
    let spacing: CGFloat = 2
    let fBlockAdditionalSpacing: CGFloat = 5
    let fitTableToCollectionView = true
    let insets = NSEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    
    override init() {
        super.init()
        sectionInset = NSEdgeInsetsMake(5, 5, 5, 5)
        minimumInteritemSpacing = 2.0
        minimumLineSpacing = 2.0
        scrollDirection = .horizontal
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sectionInset = NSEdgeInsetsMake(5, 5, 5, 5)
        minimumInteritemSpacing = 2.0
        minimumLineSpacing = 2.0
        scrollDirection = .horizontal
    }
    
    override func prepare() {
        if fitTableToCollectionView {
            let bounds = collectionView!.bounds
            size = CGSize(width: (bounds.size.width - insets.left - insets.right - CGFloat(columns + 1) * spacing) / CGFloat(columns),
                          height: (bounds.size.height - insets.top - insets.bottom - CGFloat(rows + 1) * spacing - fBlockAdditionalSpacing) / CGFloat(rows))
        } else {
            size = CGSize(width: 25, height: 30)
        }
    }
    
    // The size of the overall periodic table. This consists of 18 columns (one associated with each
    // group) and 9 rows (the 7 "periods", plus two more for the f-block lanthanides and actinides).
    
    override var collectionViewContentSize: CGSize {
        return NSSize(width: CGFloat(columns + 1) * spacing + CGFloat(columns) * size.width,
                      height: CGFloat(rows + 1) * spacing + CGFloat(rows) * size.height)
    }
    
    // For a particular `IndexPath`, this returns the `size` and `center` of the `Element` in
    // question in the collection view.
    override func layoutAttributesForItem(at indexPath: IndexPath) -> NSCollectionViewLayoutAttributes? {
        let element = elementAt(indexPath.item)
        let rect = rectFor(element)
        let attributes = NSCollectionViewLayoutAttributes(forItemWith: indexPath)
        attributes.size = size
        attributes.frame = rect
        return attributes
    }
    
    // This
    //    1. gets the array of elements
    //    2. identifies which intersect with the `CGRect` in question
    //    3. determines the `IndexPath` associated with those filtered `Element` objects
    //    4. Calls the `layoutAttributesForItem` for each of those `IndexPath` references.
    override func layoutAttributesForElements(in rect: NSRect) -> [NSCollectionViewLayoutAttributes] {
        return (collectionView?.dataSource as! ElementDataSource).elements!
            .filter { rect.intersects(self.rectFor($0)) }
            .map { IndexPath(item: $0.atomicNumber - 1, section: 0) }
            .map { self.layoutAttributesForItem(at: $0)! }
    }
    
    override func layoutAttributesForSupplementaryView(ofKind elementKind: NSCollectionView.SupplementaryElementKind, at indexPath: IndexPath) -> NSCollectionViewLayoutAttributes? {
        let attributes = NSCollectionViewLayoutAttributes(forSupplementaryViewOfKind: NSCollectionView.elementKindSectionHeader, with: IndexPath(item: 0, section: 0))
        attributes.frame = CGRect(x: 0, y: 0, width: collectionView!.frame.size.width, height: 100)
        print("hello")
        return attributes
    }
    
}

extension PeriodicTableLayout {
    
    /// Element associated particular collection view item number.
    ///
    /// - Parameter index: The item number of the collection view cell.
    /// - Returns: The `Element` associated with that cell.
    fileprivate func elementAt(_ index: Int) -> Element {
        return (collectionView?.dataSource as! ElementDataSource).elements![index]
    }
    
    /// The `CGRect` associated with a particular element.
    ///
    /// The `Element` has zero-based `row` and `column` numbers that indicate where in the
    /// periodic table the element should appear. This translates that to `CGRect` value.
    ///
    /// - Parameter element: The `Element`.
    /// - Returns: The `CGRect` associated with that `Element`.
    fileprivate func rectFor(_ element: Element) -> NSRect {
        let row = element.row
        let column = element.column
        
        let x = CGFloat(column + 1) * spacing + CGFloat(column) * size.width + insets.left
        
        var y = CGFloat(row + 1) * spacing + CGFloat(row) * size.height + insets.top
        y += row > 6 ? fBlockAdditionalSpacing : 0.0
        
        let origin = CGPoint(x: x, y: y)
        return CGRect(origin: origin, size: size)
    }
    
}
