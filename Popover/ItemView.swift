//
//  ItemView.swift
//  Popover
//
//  Created by Timothy Park on 6/7/19.
//  Copyright © 2019 Timothy Park. All rights reserved.
//

import Cocoa

class ItemView: NSView {
    
    private var trackingArea: NSTrackingArea?
    var mouseInside: Bool = false {
        didSet {
            self.needsDisplay = true
        }
    }
    
    var item: Element? {
        didSet {
            self.atomicNumberLabel.stringValue = String(item!.atomicNumber)
            self.symbolLabel.stringValue = item!.symbol
            self.atomicMassLabel.stringValue = item!.atomicWeight

            let type = item?.groupBlock
            switch type {
            case "Nonmetal":
                self.layer?.backgroundColor = Styles.nonmetals.cgColor
            case "Noble Gas":
                self.layer?.backgroundColor = Styles.noblegas.cgColor
            case "Alkali Metal":
                self.layer?.backgroundColor = Styles.alkalimetals.cgColor
            case "Alkaline Earth Metal":
                self.layer?.backgroundColor = Styles.alkalineEarthMetals.cgColor
            case "Metalloid":
                self.layer?.backgroundColor = Styles.metalloids.cgColor
            case "Halogen":
                self.layer?.backgroundColor = Styles.halogens.cgColor
            case "Metal":
                self.layer?.backgroundColor = Styles.metals.cgColor
            case "Transition Metal":
                self.layer?.backgroundColor = Styles.transitionMetals.cgColor
            case "Lanthanide":
                self.layer?.backgroundColor = Styles.lanthanides.cgColor
            case "Actinide":
                self.layer?.backgroundColor = Styles.actinides.cgColor
            case "post-transition metal":
                self.layer?.backgroundColor = Styles.postTransitionMetals.cgColor
            case "Transactinide":
                self.layer?.backgroundColor = Styles.transactinide.cgColor
            case "":
                self.layer?.backgroundColor = Styles.none.cgColor
            case .none:
                break
            case .some(_):
                break
            }
        }
    }
    
    public lazy var atomicNumberLabel: NSTextField = {
        let label = NSTextField.init(labelWithString: "")
        label.translatesAutoresizingMaskIntoConstraints = false
        label.appearance = NSAppearance.init(named: .darkAqua)
        label.font = NSFont.systemFont(ofSize: 8, weight: .regular)
        label.alignment = .left
        label.textColor = .black
        return label
    }()
    
    public lazy var symbolLabel: NSTextField = {
        let label = NSTextField.init(labelWithString: "")
        label.translatesAutoresizingMaskIntoConstraints = false
        label.appearance = NSAppearance.init(named: .darkAqua)
        label.font = NSFont.systemFont(ofSize: 10, weight: .medium)
        label.alignment = .center
        label.textColor = .black
        return label
    }()
    
    public lazy var atomicMassLabel: NSTextField = {
        let label = NSTextField.init(labelWithString: "")
        label.translatesAutoresizingMaskIntoConstraints = false
        label.appearance = NSAppearance.init(named: .darkAqua)
        label.font = NSFont.systemFont(ofSize: 6, weight: .medium)
        label.alignment = .center
        label.textColor = .black
        return label
    }()
    
    public lazy var verticalStackView: NSStackView = {
        let verticalStackView = NSStackView()
        verticalStackView.translatesAutoresizingMaskIntoConstraints = false
        verticalStackView.orientation = .vertical
        verticalStackView.distribution = .fillProportionally
        verticalStackView.spacing = 0.0
        verticalStackView.setHuggingPriority(.defaultHigh, for: .vertical)
        return verticalStackView
    }()
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        if mouseInside {
            NSColor.white.set()
            drawBorder(dirtyRect)
        }
    }
    
    func drawBorder(_ rect: NSRect) {
        let textViewSurround = NSBezierPath(roundedRect: bounds, xRadius: 1.5, yRadius: 1.5)
        textViewSurround.lineWidth = 3
        textViewSurround.stroke()
    }
    
    override func updateTrackingAreas() {
        super.updateTrackingAreas()
        ensureTrackingArea()
        if !trackingAreas.contains(trackingArea!) {
            addTrackingArea(trackingArea!)
        }
    }
    
    func ensureTrackingArea() {
        if trackingArea == nil {
            trackingArea = NSTrackingArea(rect: bounds, options: [.inVisibleRect, .activeAlways, .mouseEnteredAndExited], owner: self, userInfo: nil)
        }
    }
    
    override func mouseEntered(with event: NSEvent) {
        mouseInside = true
    }
    
    override func mouseExited(with event: NSEvent) {
        mouseInside = false
    }
    
    override init(frame frameRect: NSRect) {
        super.init(frame: NSRect.zero)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        self.wantsLayer = true
        self.addSubview(verticalStackView)
        verticalStackView.addView(atomicNumberLabel, in: .top)
        verticalStackView.addView(symbolLabel, in: .center)
        verticalStackView.addView(atomicMassLabel, in: .bottom)
        
        NSLayoutConstraint.activate([
            verticalStackView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            verticalStackView.topAnchor.constraint(equalTo: self.topAnchor),
            verticalStackView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            verticalStackView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
            ]
        )
    }
    
}
