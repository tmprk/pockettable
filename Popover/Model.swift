//
//  Model.swift
//  Popover
//
//  Created by Timothy Park on 6/7/19.
//  Copyright © 2019 Timothy Park. All rights reserved.
//

import Cocoa

struct Element {
    let atomicNumber: Int
    let symbol: String
    let element: String
    let group: Int? // 1-based group number (corresponding to the column in the table), from 1 to 18 (but f-block lanthanides and actinides have no `group` value
    let period: Int // 1-based row number (corresponding to the row in the table), from 1 to 7 (note, f-block lanthanides and actinides will be visually represented on separate rows in table)
    let atomicWeight: String
    let groupBlock: String
    var phase: String
    var type: String
    var ionicRadius: String
    var atomicRadius: String
    var electronegativity: String
    var firstIonizationPotential: String
    var density: String
    var meltingPointK: String
    var boilingPointK: String
    var isotopes: String
    var specificHeatCapacity: String
    var electronConfiguration: String
    
    init(dictionary: [String: Any]) {
        atomicNumber = Int(dictionary["atomicNumber"] as! String)!
        element = dictionary["element"] as! String
        symbol = dictionary["symbol"] as! String
        atomicWeight = dictionary["atomicweight"] as! String
        period = Int(dictionary["period"] as! String)!
        group = Int(dictionary["group"] as! String)
        groupBlock = dictionary["type"] as! String
        phase = dictionary["phase"] as! String
        type = dictionary["type"] as! String
        ionicRadius = dictionary["ionicRadius"] as! String
        atomicRadius = dictionary["atomicRadius"] as! String
        electronegativity = dictionary["electronegativity"] as! String
        firstIonizationPotential = dictionary["firstIonizationPotential"] as!  String
        density = dictionary["density"] as!  String
        meltingPointK = dictionary["meltingPointK"] as! String
        boilingPointK = dictionary["boilingPointk"] as! String
        isotopes = dictionary["isotopes"] as! String
        specificHeatCapacity = dictionary["specificHeatCapacity"] as! String
        electronConfiguration = dictionary["electronConfiguration"] as! String
    }
    
    /// A zero-based row number.
    ///
    /// This largely corresponds to the 1-based `period`, though the f-block elements
    /// have been moved down below the main table and have their own rows.
    
    var row: Int {
        switch atomicNumber {
        case 57 ... 71:             // f-block lan­thanides
            return 8 - 1
        case 89 ... 103:            // f-block actinides
            return 9 - 1
        default:
            return period - 1
        }
    }
    
    /// A zero-based column number.
    ///
    /// This largely corresponds to the 1-based `group`, though the f-block elements
    /// have been moved down below the main table and show up, starting under the `group`
    /// 3 elements.
    
    var column: Int {
        switch atomicNumber {
        case 57 ... 71:             // f-block lan­thanides
            return atomicNumber - 57 + 2
        case 89 ... 103:            // f-block actinides
            return atomicNumber - 89 + 2
        default:
            return group! - 1
        }
    }
}
