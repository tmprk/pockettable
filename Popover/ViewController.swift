//
//  ViewController.swift
//  Popover
//
//  Created by Timothy Park on 6/7/19.
//  Copyright © 2019 Timothy Park. All rights reserved.
//

import Cocoa

class ViewController: NSViewController, ElementDataSource {
    
    var elements: [Element]?
    var scrollView: NSScrollView!
    var collectionView: NSCollectionView!
    var periodicTableLayout = PeriodicTableLayout()
    var eventMonitor: EventMonitor?
    var button: NSButton? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadElements()
        setupCollectionView()
        setupScrollView()
        detectInterface()
        listenForInterfaceChange()
        setupButton()
        // stuff you can configure
        view.wantsLayer = false
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()
        self.view.window?.title = "Window"
    }

    private func loadElements() {
        if let fileURL = Bundle.main.url(forResource: "PeriodicTable", withExtension: "json") {
            do {
                let data = try! Data(contentsOf: fileURL)
                let json = try! JSONSerialization.jsonObject(with: data) as! [[String: Any]]
                self.elements = json.map { Element(dictionary: $0) }
            }
        }
    }
    
    func listenForInterfaceChange() {
        DistributedNotificationCenter.default.addObserver(
            self,
            selector: #selector(detectInterface),
            name: .AppleInterfaceThemeChangedNotification,
            object: nil
        )
    }
    
    func setupCollectionView() {
        collectionView = NSCollectionView(frame: .zero)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.collectionViewLayout = periodicTableLayout
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(ElementItem.self, forItemWithIdentifier: ElementItem.identifier)
        
        // stuff you can configure
        collectionView.backgroundView?.wantsLayer = false
        collectionView.wantsLayer = false
        collectionView.allowsEmptySelection = true
        collectionView.allowsMultipleSelection = false
        collectionView.isSelectable = true
        collectionView.autoresizingMask = NSView.AutoresizingMask([.width, .maxXMargin, .minYMargin, .height, .maxYMargin])
    }
    
    func setupScrollView() {
        scrollView = NSScrollView(frame: view.frame)
        scrollView.documentView = collectionView
        view.addSubview(scrollView)
        self.scrollView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            self.scrollView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            self.scrollView.topAnchor.constraint(equalTo: self.view.topAnchor),
            self.scrollView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            self.scrollView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
            ]
        )
        
        // stuff you can configure
        scrollView.drawsBackground = false
        scrollView.contentView.drawsBackground = false
        scrollView.wantsLayer = false
    }
    
    func setupButton() {
        button = NSButton(image: NSImage(named: "settingsIcon")!, target: self, action: #selector(showMenu))
        button?.isBordered = false
        button?.appearance = NSAppearance.init(named: .darkAqua)
        button?.translatesAutoresizingMaskIntoConstraints = false
        collectionView.addSubview(button!)
        NSLayoutConstraint.activate([
            self.button!.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -7.5),
            self.button!.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -7.5)
            ]
        )
    }
    
    @objc func detectInterface() {
        switch view.isDarkMode {
        case true:
            print("it's dark mode")
            collectionView.backgroundColors = [Styles.darkBackgroundColor]
        case false:
            print("it's light mode")
            collectionView.backgroundColors = [Styles.lightBackgroundColor]
        }
    }
    
    @objc func showMenu() {
        let menu = NSMenu(title: "Options Menu")
        menu.insertItem(withTitle: NSLocalizedString("About Periodic Table", comment: ""), action: #selector(printHello), keyEquivalent: "", at: 0)
        menu.insertItem(withTitle: NSLocalizedString("Check for Updates...", comment: ""), action: #selector(printHello), keyEquivalent: "", at: 1)
        menu.insertItem(NSMenuItem.separator(), at: 2)
        menu.insertItem(withTitle: NSLocalizedString("Preferences...", comment: ""), action: #selector(showPrefs(_:)), keyEquivalent: ",", at: 3)
        menu.insertItem(NSMenuItem.separator(), at: 4)
        menu.insertItem(withTitle: NSLocalizedString("Quit Periodic Table", comment: ""), action: #selector(printHello), keyEquivalent: "q", at: 5)
        let pt: NSPoint = NSOffsetRect(view.frame, 0, -5).origin
        menu.popUp(positioning: nil, at: pt, in: view)
    }
    
    @objc func showPrefs(_ sender: Any?) {
        NSApplication.shared.activate(ignoringOtherApps: true)
    }
    
    @objc func printHello() {
        print("hello")
    }
    
}

extension ViewController: NSCollectionViewDelegate, NSCollectionViewDataSource {
    
    func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        return elements?.count ?? 0
    }
    
    func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        guard let cell = collectionView.makeItem(withIdentifier: ElementItem.identifier, for: indexPath) as? ElementItem else {
            fatalError("Couldn't get a valid NSCollectionViewItem. Did the type change or something...?")
        }
        let item = elements![indexPath.item]
        cell.itemView?.item = item
        return cell
    }
    
    func collectionView(_ collectionView: NSCollectionView, didSelectItemsAt indexPaths: Set<IndexPath>) {
        guard let first = indexPaths.first else { return }
        let element = elements![first.item]
        print(element.element)
    }
    
}

extension ViewController : NSCollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: NSCollectionView, layout collectionViewLayout: NSCollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> NSSize {
        return NSSize(width: collectionView.bounds.width, height: 40)
    }
}
