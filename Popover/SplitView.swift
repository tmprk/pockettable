//
//  SplitView.swift
//  Popover
//
//  Created by Timothy Park on 6/10/19.
//  Copyright © 2019 Timothy Park. All rights reserved.
//

import Cocoa
import Foundation

class SplitView: NSSplitView {
    
    var cursor: NSCursor = .closedHand {
        didSet {
            resetCursorRects()
        }
    }
    
    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var dividerThickness:CGFloat {
        return 100
    }

    override func resetCursorRects() {
        super.resetCursorRects()
        self.addCursorRect(self.bounds, cursor: cursor)
    }

}
