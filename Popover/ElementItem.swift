//
//  ElementItem.swift
//  Popover
//
//  Created by Timothy Park on 6/7/19.
//  Copyright © 2019 Timothy Park. All rights reserved.
//

import Cocoa

class ElementItem: NSCollectionViewItem {
    
    var itemView: ItemView?
    
    override func loadView() {
        self.itemView = ItemView(frame: NSZeroRect)
        self.view = self.itemView!
        self.view.postsFrameChangedNotifications = false
        self.view.postsBoundsChangedNotifications = false
    }
    
    override func viewDidLayout() {
        super.viewDidLayout()
        view.layer?.cornerRadius = 1.5
    }
    
    func getView() -> ItemView {
        return self.itemView!
    }
    
    static var identifier: NSUserInterfaceItemIdentifier {
        let stringOfSelf = String(describing: self)
        return NSUserInterfaceItemIdentifier(rawValue: stringOfSelf)
    }
    
}
