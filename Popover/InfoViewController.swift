//
//  InfoViewController.swift
//  Popover
//
//  Created by Timothy Park on 6/9/19.
//  Copyright © 2019 Timothy Park. All rights reserved.
//

import Cocoa

class InfoViewController: NSViewController {
    
    @IBOutlet weak var outlineView: NSOutlineView!
    var tableViewCellForSizing: NSTableCellView?
    
    var element = Element(dictionary: [
        "atomicNumber":"1",
        "element":"Hydrogen",
        "symbol":"H",
        "atomicweight":"1.00794",
        "period":"1",
        "group":"1",
        "phase":"gas",
        "type":"Nonmetal",
        "ionicRadius":"0.012",
        "atomicRadius":"0.79",
        "electronegativity":"2.2",
        "firstIonizationPotential":"13.5984",
        "density":"0.00008988",
        "meltingPointK":"14.175",
        "boilingPointk":"20.28",
        "isotopes":"3",
        "specificHeatCapacity":"14.304",
        "electronConfiguration":"1s1"
        ]
    )
    
    let keys = ["atomicNumber",
                "element",
                "symbol",
                "atomicWeight",
                "period",
                "group",
                "phase",
                "type",
                "ionicRadius",
                "atomicRadius",
                "electronegativity",
                "firstIonizationPotential",
                "density",
                "meltingPointK",
                "boilingPointK",
                "isotopes",
                "specificHeatCapacity",
                "electronConfiguration"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        outlineView.dataSource = self
        outlineView.delegate = self
        outlineView.allowsColumnResizing = true
    }
}

extension InfoViewController: NSOutlineViewDataSource, NSOutlineViewDelegate {
    
    // You must give each row a unique identifier, referred to as `item` by the outline view
    //   * For top-level rows, we use the values in the `keys` array
    //   * For the hobbies sub-rows, we label them as ("hobbies", 0), ("hobbies", 1), ...
    //     The integer is the index in the hobbies array
    //
    // item == nil means it's the "root" row of the outline view, which is not visible
    
    func outlineView(_ outlineView: NSOutlineView, didAdd rowView: NSTableRowView, forRow row: Int) {
        print(rowView.fittingSize.height)
    }
    
    func outlineView(_ outlineView: NSOutlineView, child index: Int, ofItem item: Any?) -> Any {
        if item == nil {
            return keys[index]
        } else {
            return 0
        }
    }
    
    func displayStringHeight(withWidth width: CGFloat, string: String) -> CGFloat {
        let size = NSMakeSize(width, 0)
        let bounds = string.boundingRect(with: size, options: [.usesLineFragmentOrigin, .usesFontLeading])
        return bounds.size.height
    }
    
    // Tell how many children each row has:
    //    * The root row has 5 children: name, age, birthPlace, birthDate, hobbies
    //    * The hobbies row has how ever many hobbies there are
    //    * The other rows have no children
    func outlineView(_ outlineView: NSOutlineView, numberOfChildrenOfItem item: Any?) -> Int {
        if item == nil {
            return keys.count
        } else {
            return 0
        }
    }
    
    // Tell whether the row is expandable. The only expandable row is the Hobbies row
    func outlineView(_ outlineView: NSOutlineView, isItemExpandable item: Any) -> Bool {
        return false
    }
    
    // Set the text for each row
    func outlineView(_ outlineView: NSOutlineView, viewFor tableColumn: NSTableColumn?, item: Any) -> NSView? {
        guard let columnIdentifier = tableColumn?.identifier.rawValue else {
            return nil
        }
        
        var text = ""
        
        // Recall that `item` is the row identiffier
        switch (columnIdentifier, item) {
        case ("keyColumn", let item as String):
            switch item {
            case "atomicNumber":
                text = "Atomic #"
            case "element":
                text = "Name"
            case "symbol":
                text = "Symbol"
            case "atomicWeight":
                text = "Mass"
            case "period":
                text = "Period"
            case "group":
                text = "Group"
            case "phase":
                text = "Phase"
            case "type":
                text = "Type"
            case "ionicRadius":
                text = "Ionic Radius"
            case "atomicRadius":
                text = "Atomic Radius"
            case "electronegativity":
                text = "Electronegativity"
            case "firstIonizationPotential":
                text = "Ionization Energy"
            case "density":
                text = "Density"
            case "meltingPointK":
                text = "Melting Point"
            case "boilingPointK":
                text = "Boiling Point"
            case "isotopes":
                text = "Isotopes"
            case "specificHeatCapacity":
                text = "Heat Capacity"
            case "electronConfiguration":
                text = "Electron Configuration"
            default:
                break
            }
        case ("valueColumn", let item as String):
            switch item {
            case "atomicNumber":
                text = "\(element.atomicNumber)"
            case "element":
                text = element.element
            case "symbol":
                text = element.symbol
            case "atomicWeight":
                text = element.atomicWeight
            case "period":
                text = "\(element.period)"
            case "group":
                text = "\(element.group!)"
            case "phase":
                text = element.phase
            case "type":
                text = element.type
            case "ionicRadius":
                text = element.ionicRadius
            case "atomicRadius":
                text = element.atomicRadius
            case "electronegativity":
                text = element.electronegativity
            case "firstIonizationPotential":
                text = element.firstIonizationPotential
            case "density":
                text = element.density
            case "meltingPointK":
                text = element.meltingPointK
            case "boilingPointK":
                text = element.density
            case "isotopes":
                text = "\(element.isotopes)"
            case "specificHeatCapacity":
                text = element.specificHeatCapacity
            case "electronConfiguration":
                text = element.electronConfiguration
            default:
                break
            }
        default:
            text = ""
        }
        
        let cellIdentifier = NSUserInterfaceItemIdentifier("outlineViewCell")
        let cell = outlineView.makeView(withIdentifier: cellIdentifier, owner: self) as! NSTableCellView
        cell.textField!.stringValue = text
        cell.textField?.alignment = .center
        cell.textField?.lineBreakMode = .byWordWrapping
        cell.textField?.maximumNumberOfLines = 0
        return cell
    }
    
}
