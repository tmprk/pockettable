//
//  NSNotification.swift
//  Popover
//
//  Created by Timothy Park on 6/7/19.
//  Copyright © 2019 Timothy Park. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let AppleInterfaceThemeChangedNotification = Notification.Name("AppleInterfaceThemeChangedNotification")
}
