//
//  Styles.swift
//  Popover
//
//  Created by Timothy Park on 6/7/19.
//  Copyright © 2019 Timothy Park. All rights reserved.
//

import Cocoa

struct Styles {
    static let darkBackgroundColor = #colorLiteral(red: 0.2196078431, green: 0.2078431373, blue: 0.2156862745, alpha: 1)
    static let lightBackgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    static let alkalineEarthMetals = #colorLiteral(red: 0.8572789581, green: 0.8670012282, blue: 0, alpha: 1)
    static let nonmetals = #colorLiteral(red: 0.1909943829, green: 0.7778115112, blue: 0.1684086965, alpha: 1)
    static let noblegas = #colorLiteral(red: 0.1916780549, green: 0.6132878814, blue: 0.9219660163, alpha: 1)
    static let metalloids = #colorLiteral(red: 0.5945537686, green: 0.8988236785, blue: 0.6424139142, alpha: 1)
    static let halogens = #colorLiteral(red: 0.1909943829, green: 0.7778115112, blue: 0.1684086965, alpha: 1)
    static let alkalimetals = #colorLiteral(red: 0.9219660163, green: 0.5266444087, blue: 0.2199499309, alpha: 1)
    static let metals = #colorLiteral(red: 0.5620505214, green: 0.8912140727, blue: 0.6181745529, alpha: 1)
    static let lanthanides = #colorLiteral(red: 0.8326485348, green: 0.6616400533, blue: 0.5822358283, alpha: 1)
    static let actinides = #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1)
    static let transitionMetals = #colorLiteral(red: 0.7332743985, green: 0.5785932313, blue: 0.7410091849, alpha: 1)
    static let postTransitionMetals = #colorLiteral(red: 0.6757845879, green: 0.8918730617, blue: 0.8391833901, alpha: 1)
    static let transactinide = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
    static let none = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
}
